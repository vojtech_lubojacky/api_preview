<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RestRoute;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
		$router[] = new RestRoute('api[/<presenter>[/<id>]]', array(
			'presenter' => 'Api',
			'action' => array(
				RestRoute::GET => 'default',
				RestRoute::PUT => 'update',
				RestRoute::POST => 'create',
				RestRoute::DELETE => 'delete',
			),
			'id' => NULL,
		), RestRoute::GET | RestRoute::PUT | RestRoute::POST | RestRoute::DELETE);
		//$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
		return $router;
	}

}
