<?php

class UserRepository extends Nette\Object
{
	private $db;

	private $table = 'user';

	private $allowedUpdateData = array(
		'login', 'email'
	);

	private $allowedCreateData = array(
		'login', 'email'
	);

	public function __construct(Nette\Database\Context $db)
	{
		$this->db = $db;
	}

	public function getAll()
	{
		return $this->db->table($this->table)->fetchAll();
	}

	public function getById($id)
	{
		return $this->db->table($this->table)->where('id = ?', $id)->fetch();
	}

	public function delete($id)
	{
		$this->db->table($this->table)->where('id = ?', $id)->delete();
	}

	public function update($id, $rawData)
	{
		$data = array();

		foreach($this->allowedUpdateData as $allowedIndex)
		{
			if(isset($rawData[$allowedIndex]) && $rawData[$allowedIndex])
			{
				$data[$allowedIndex] = $rawData[$allowedIndex];
			}
		}

		if(!empty($data))
		{
			$this->db->table($this->table)->where('id = ?', $id)->update($data);
		}
	}

	public function create($rawData)
	{
		$data = array();

		foreach($this->allowedCreateData as $allowedIndex)
		{
			if(isset($rawData[$allowedIndex]) && $rawData[$allowedIndex])
			{
				$data[$allowedIndex] = $rawData[$allowedIndex];
			}
		}

		if(!empty($data))
		{
			return $this->db->table($this->table)->insert($data);
		}

		return NULL;
	}
}
