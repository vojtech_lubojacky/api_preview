## Init: ##


1) set DB connection in config.local.neon

2) run db_init.sql in yout MySQL (default name "api_preview")


## Calls: ##

GET /api/user - user list

GET /api/user/{id} - user detail

POST /api/user - create new user and return info

PUT /api/user/{id} - update user by id

DELETE /api/user/{id} - remove user by id