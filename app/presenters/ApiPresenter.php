<?php

namespace App\Presenters;

use Nette;


class ApiPresenter extends Nette\Application\UI\Presenter
{
	private $version = 1.0;

	protected $response = array();
	protected $status = 'SUCESS';
	protected $statusMessage = '';

	public function actionDefault()
	{
		$this->prepareResponse();

		$this->sendJson($this->response);
	}

	protected function prepareResponse()
	{
		$this->response['version'] = $this->version;
		$this->response['status'] = $this->status;
		$this->response['message'] = $this->statusMessage;
		$this->response['links'] = array(
			array(
				'href' => '/user',
				'rel' => 'list',
				'method' => 'GET',
			),
			array(
				'href' => '/user',
				'rel' => 'create',
				'method' => 'POST',
			),
		);
	}
}
