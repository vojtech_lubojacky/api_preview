<?php

namespace App\Presenters;

use Nette;


class UserPresenter extends ApiPresenter
{
	/**
	 * @var \UserRepository
	 */
	private $userRepository;

	public function __construct(\UserRepository $repository)
	{
		$this->userRepository = $repository;
		parent::__construct();
	}

	public function actionDefault()
	{
		$id = $this->getParameter('id');
		$this->prepareResponse($id);
		$this->sendJson($this->response);
	}

	public function actionUpdate()
	{
		$id = $this->getParameter('id');
		$data = $this->request->post;

		$this->userRepository->update($id, $data);

		$this->prepareResponse($id);
		$this->sendJson($this->response);
	}

	public function actionCreate()
	{
		$data = $this->request->post;

		$id = $this->getParameter('id');
		if($id)
		{
			$this->status = 'ERROR';
			$this->statusMessage = 'Not allowed opration on User ID '.$id;
			parent::prepareResponse();
			$this->sendJson($this->response);
			exit();
		}

		$id = $this->userRepository->create($data);

		if(!$id)
		{
			$this->status = 'ERROR';
			$this->statusMessage = 'User was not created, check data and try again.';
			parent::prepareResponse();
			$this->sendJson($this->response);
			exit();
		}
		else
		{
			$this->prepareResponse($id);
		}

		$this->sendJson($this->response);
	}


	public function actionDelete()
	{
		$id = $this->getParameter('id');

		$this->userRepository->delete($id);

		parent::prepareResponse();
		$this->sendJson($this->response);
	}

	protected function prepareResponse($id = NULL)
	{
		if($id)
		{
			$user = $this->userRepository->getById($id);

			if($user)
			{
				$this->prepareResponseUser($user);
			}
			else
			{
				$this->status = 'ERROR';
				$this->statusMessage = 'User ID '.$id.' does not exist.';
			}

		}
		else
		{
			$users = $this->userRepository->getAll();
			foreach($users as $user)
			{
				$this->prepareResponseUser($user);
			}
		}

		parent::prepareResponse();
	}

	private function prepareResponseUser($data)
	{
		if(!isset($this->response['users']))
		{
			$this->response['users'] = array();
		}

		$this->response['users'][] = array(
			'id' => $data['id'],
			'login' => $data['login'],
			'links' => array(
				array(
					'href' => '/user/'.$data['id'],
					'rel' => 'self',
					'method' => 'GET',
				),
				array(
					'href' => '/user/'.$data['id'],
					'rel' => 'update',
					'method' => 'PUT',
				),
				array(
					'href' => '/user/'.$data['id'],
					'rel' => 'delete',
					'method' => 'DELETE',
				),
			),
		);
	}
}
