<?php
namespace Nette\Application\Routers;

use Nette\Http;
use Nette\Application;
use Nette\Utils\Strings;
use Nette\Application\Routers\Route;

class RestRoute extends Route
{
	protected $actionDictionary;
	private $methodDictionary = array(
		Http\IRequest::GET => self::GET,
		Http\IRequest::POST => self::POST,
		Http\IRequest::PUT => self::PUT,
		Http\IRequest::DELETE => self::DELETE,
	);

	const GET = 4;
	const POST = 8;
	const PUT = 16;
	const DELETE = 32;
	const RESTFUL = 60;

	/**
	 * @param string $mask
	 * @param array|string $metadata
	 * @param int $flags
	 */
	public function __construct($mask, $metadata = array(), $flags = IResourceRouter::GET)
	{
		$this->actionDictionary = array();
		if (isset($metadata['action']) && is_array($metadata['action'])) {
			$this->actionDictionary = $metadata['action'];
			$metadata['action'] = 'default';
		} else {
			$action = isset($metadata['action']) ? $metadata['action'] : 'default';
			if (is_string($metadata)) {
				$metadataParts = explode(':', $metadata);
				$action = end($metadataParts);
			}
			foreach ($this->methodDictionary as $methodName => $methodFlag) {
				if (($flags & $methodFlag) == $methodFlag) {
					$this->actionDictionary[$methodFlag] = $action;
				}
			}
		}
		parent::__construct($mask, $metadata, $flags);
	}

	public function isMethod($method)
	{
		$common = array(self::RESTFUL);
		$isActionDefined = $this->actionDictionary && !in_array($method, $common) ?
			isset($this->actionDictionary[$method]) :
			TRUE;
		return ($this->getFlags() & $method) == $method && $isActionDefined;
	}

	public function getMethod(Http\IRequest $httpRequest)
	{
		$method = $httpRequest->getMethod();
		if (!isset($this->methodDictionary[$method])) {
			return NULL;
		}
		return $this->methodDictionary[$method];
	}

	public function getActionDictionary()
	{
		return $this->actionDictionary;
	}

	public function setActionDictionary($actionDictionary)
	{
		$this->actionDictionary = $actionDictionary;
		return $this;
	}

	public function match(Http\IRequest $httpRequest)
	{
		$appRequest = parent::match($httpRequest);
		if (!$appRequest) {
			return NULL;
		}
		// Check requested method
		$methodFlag = $this->getMethod($httpRequest);

		if (!$this->isMethod($methodFlag)) {
			return NULL;
		}
		// If there is action dictionary, set method
		if ($this->actionDictionary) {
			$parameters = $appRequest->getParameters();
			$parameters['action'] = $this->actionDictionary[$methodFlag];
			$parameters['action'] = self::formatActionName($this->actionDictionary[$methodFlag], $parameters);
			$appRequest->setParameters($parameters);
		}
		return $appRequest;
	}

	protected static function formatActionName($action, array $parameters)
	{
		return Strings::replace($action, "@\<([0-9a-zA-Z_-]+)\>@i", function($m) use($parameters) {
			$key = strtolower($m[1]);
			return isset($parameters[$key]) ? $parameters[$key] : '';
		});
	}
}